package test;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ValidationTestCases {
	
	WebDriver driver;
	String url = "https://www.facebook.com";
	
	@BeforeTest
	public void setup() {
		//open the browser and url
		//open chrome browser
		driver = new ChromeDriver();
		
		//open the url
		driver.get(url);
		
	}
	
	@AfterTest
	public void tearDown() {
		//quit the browser
		driver.quit();
	}
	
	@Test
	public void pageTitleValidation() {
		String expectedResult = "Facebook – log in or sign up";
		String actualResult = driver.getTitle();
		Reporter.log("Expected Result - " + expectedResult);
		Reporter.log("Actual Result - " + actualResult);
		assertTrue(actualResult.equals(expectedResult), "Mismatch in the page title");
	}
	
	@Test
	public void urlValidtion() {
		String expectedResult = "https://www.facebook.com/";
		String actualResult = driver.getCurrentUrl();
		Reporter.log("Expected Result - " + expectedResult);
		Reporter.log("Actual Result - " + actualResult);
		assertTrue(actualResult.equals(expectedResult), "Mismatch in the page URL");
	}
	
	

}
